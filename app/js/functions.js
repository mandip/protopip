// Browser detection for when you get desparate. A measure of last resort.

// http://rog.ie/post/9089341529/html5boilerplatejs
// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }

// Uncomment the below to use:
// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

$(document).ready(function() {
	$("#search input").on({
		focus: function() {
			$(this).css("border-color", "cyan").css("border-top-width", "1px");
			$(this).prev().css("border-bottom-width", "0");
		},
		blur: function() {
			$(this).css("border-color", "#444");
			if ($(this).index()) {
				$(this).css("border-top-width", "0");
				$(this).prev().css("border-bottom-width", "1px");
			}
		}
	});
});